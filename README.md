# rosa
this is the best rust-based operating system you've ever seen

# Build Instructions
Install nightly toolchain with `rustup toolchain install nightly` followed by `rustup default nightly`

Add required component with `rustup component add llvm-tools-preview`

Install bootimage with `cargo install bootimage`

Run with `cargo run`

# Features
- String input and output (including color!)
    - Press F12 at any time to dump the input buffer to the screen
- A simple shell with a few commands
    - `echo <text>` - Print text to the screen
    - `lock <password>` - Locks the screen with a password
    - `clear` - Clear the screen
    - `baboonseeker` or `bs` - Play baboonseeker
    - `reboot` - Restart your computer
    - `rosafetch` - Utility like neofetch/pfetch
    - `date` - Show the current date and time
      - `date [timestamp]` - Convert a unix timestamp to a datetime and print it
    - `udate` - Get the unix timestamp
    - `uptime` - Get current system uptime
    - `tz <time zone>` - Set the time zone as an integer representing the difference from GMT in hours (assuming your system clock is set to GMT, if it is set to local time, you can leave this set to the default value of zero)
    - `calc` - Very simple RPN calculator

## Commands for testing purposes
- `div_zero_fault` - Crash the system by diving by zero
- `pointer_fault` - Crash the system by doing invalid pointer things
