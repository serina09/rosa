#![no_std]
#![no_main]

extern crate alloc;

use alloc::vec::Vec;
use core::panic::PanicInfo;
use hashbrown::HashMap;
use rosa::{
    applications::{baboon_seeker, calculator, date, debug, io, reboot, rosafetch, screen_locker},
    clear_screen,
    clock::{self},
    println, set_color,
    vga_buffer::Color,
};

// panic handler
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    // Print fancy error screen
    set_color!(Color::White, Color::LightRed);
    clear_screen!();
    println!("Rosa has encountered an error and cannot continue\n\n");
    println!("{}", info);
    rosa::hlt_loop();
}

fn merge_maps<K, V>(target: &mut HashMap<K, V>, source: HashMap<K, V>)
where
    K: core::hash::Hash,
    K: core::cmp::Eq,
{
    for (key, value) in source {
        target.insert(key, value);
    }
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    println!("Starting Rosa...");
    println!("Press F12 at any time to dump input buffer");

    rosa::init();

    // Start counting uptime
    let _ = clock::uptime::get_uptime();

    let mut commands = HashMap::new();
    merge_maps(&mut commands, baboon_seeker::get_commands());
    merge_maps(&mut commands, calculator::get_commands());
    merge_maps(&mut commands, date::get_commands());
    merge_maps(&mut commands, debug::get_commands());
    merge_maps(&mut commands, io::get_commands());
    merge_maps(&mut commands, reboot::get_commands());
    merge_maps(&mut commands, rosafetch::get_commands());
    merge_maps(&mut commands, screen_locker::get_commands());

    //println!("yay");

    // WRITER.lock().set_color(Color::Red, Color::Black);
    //set_color!(Color::Red, Color::Black);
    //println!("test");
    loop {
        set_color!(Color::Red, Color::Black);
        let input_text = rosa::input("Rosa Shell> ", Some([Color::White, Color::Black]));

        let command: Vec<&str> = input_text.split(' ').collect();

        if commands.contains_key(command[0]) {
            commands.get(command[0]).unwrap()(&command[1..]);
        } else if command[0].is_empty() {
            // Ignore empty command
        } else if command[0] == "help" {
            // List available commands
            println!("Available commands: ");
            for command in &commands {
                println!("{}", command.0);
            }
        } else {
            println!("Command {} not found", command[0]);
        }

        // TODO: add the rest of the commands as apps

        // match command[0] {
        //     "echo" => {
        //         if command.len() > 1 {
        //             for text in command.iter().skip(1) {
        //                 print!("{} ", text);
        //             }
        //             print!("\n");
        //         } else {
        //             println!("Requires text to print!")
        //         }
        //     }
        //     "lock" => {
        //         applications::screen_locker::main(command, input_text.clone());
        //     }
        //     "clear" => clear_screen!(),
        //     "baboonseeker" | "bs" => {
        //         applications::baboon_seeker::main();
        //     }
        //     "reboot" => applications::reboot::reboot(),
        //     "rosafetch" => {
        //         applications::rosafetch::main();
        //     }
        //     "date" => {
        //         if command.len() == 1 {
        //             println!("{}", clock::get_current_time());
        //         } else {
        //             println!(
        //                 "{}",
        //                 DateTime::from_unix_timestamp(command[1].parse::<u64>().unwrap())
        //             );
        //         }
        //     }
        //     "udate" => {
        //         println!("{}", clock::get_current_time().to_unix_timestamp())
        //     }
        //     "uptime" => {
        //         clock::uptime::print_uptime();
        //     }
        //     "tz" => {
        //         clock::tz_command(command);
        //     }
        //     "calc" => applications::calculator::calculator(),
        //     "div_zero_fault" => {
        //         unsafe {
        //             core::arch::asm!(
        //                 "
        //                 mov ax, 0
        //                 mov bl, 0
        //                 div bl
        //             "
        //             )
        //         };
        //     }
        //     "pointer_fault" => {
        //         unsafe {
        //             *(0xdeadbeaf as *mut u64) = 42;
        //         };
        //     }
        //     "" => (),
        //     _ => println!("Command {} not found", command[0]),
        // }
        //println!("{:?}", rosa::input());
    }

    // rosa::hlt_loop();
}
