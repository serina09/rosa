// Disable standard library features and enable x86 interrupt ABI feature
#![no_std]
#![feature(abi_x86_interrupt)]
#![feature(const_mut_refs)]
#![feature(allocator_api)]

extern crate alloc;

use alloc::{string::String, vec::Vec};
use cursor_control::enable_cursor;
// Import modules
use vga_buffer::Color;

pub mod allocator;
pub mod applications;
pub mod clock;
pub mod cpuio;
pub mod cursor_control;
pub mod gdt;
pub mod interrupts;
pub mod vga_buffer;

// Global history buffer using a spin lock
pub static HISTORY: spin::Mutex<Vec<String>> = spin::Mutex::new(Vec::new());

// Initialize the system components (Global Descriptor Table, Interrupt Descriptor Table, etc.)
pub fn init() {
    gdt::init();
    interrupts::init_idt();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();
    enable_cursor(0, 15);
    // Disable blinking text
    unsafe {
        core::arch::asm!(
            "
            mov dx, 0x3DA
            in al, dx
            mov dx, 0x3C0
            mov al, 0x30
            out dx, al
            inc dx
            in al, dx
            and al, 0xF7
            dec dx
            out dx, al
        "
        )
    }
}

// Main loop that repeatedly halts the CPU
pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}

// Function to get user input with a prompt and optional text colors
pub fn input(prompt: &str, colors: Option<[Color; 2]>) -> String {
    // Print the prompt
    print!("{}", prompt);

    // Set text colors if provided
    if let Some(c) = colors {
        set_color!(c[0], c[1]);
    }

    // Wait for user input
    while !*interrupts::USER_INPUT_DONE.lock() {
        x86_64::instructions::hlt();
    }

    // Reset user done variable now that we are handling the input
    let mut guard = interrupts::USER_INPUT_DONE.lock();
    *guard = false;

    // Retrieve input from the buffer
    let input_string = interrupts::INPUT_BUFFER.lock().clone();

    // Clear the input buffer
    interrupts::INPUT_BUFFER.lock().clear();

    // Print a newline for better formatting
    println!();

    // Don't add obscured inputs to history
    if !*interrupts::OBSCURE_INPUT.lock() {
        // Limit the history buffer size and store the input
        if HISTORY.lock().len() > 127 {
            HISTORY.lock().remove(0);
        }
        HISTORY.lock().push(input_string.clone());
    }

    // Return the user input
    input_string
}
