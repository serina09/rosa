use crate::{
    cpuio::{inb, outb},
    vga_buffer::BUFFER_WIDTH,
};

/// Function for enabling the cursor with specified start and end positions
pub fn enable_cursor(cursor_start: u8, cursor_end: u8) {
    unsafe {
        outb(0x0A, 0x3D4);
        outb((inb(0x3D5) & 0xC0) | cursor_start, 0x3D5);

        outb(0x0B, 0x3D4);
        outb((inb(0x3D5) & 0xE0) | cursor_end, 0x3D5);
    }
}

/// Function for disabling the cursor
pub fn disable_cursor() {
    unsafe {
        outb(0x0A, 0x3D4);
        outb(0x20, 0x3D5);
    }
}

/// Function for updating the cursor position
pub fn update_cursor(x: usize, y: usize) {
    let pos = y * BUFFER_WIDTH + x;
    unsafe {
        outb(0x0F, 0x3D4);
        outb((pos & 0xFF) as u8, 0x3D5);
        outb(0x0E, 0x3D4);
        outb(((pos >> 8) & 0xFF) as u8, 0x3D5);
    }
}

/// Function for getting the cursor position
pub fn get_cursor_position() -> u16 {
    let mut pos: u16 = 0;
    unsafe {
        outb(0x0F, 0x3D4);
        pos |= inb(0x3D5) as u16;
        outb(0x0E, 0x3D4);
        pos |= (inb(0x3D5) as u16) << 8;
    }
    pos
}
