//! VGA Buffer Module
//!
//! This module provides functionality for writing to the VGA text buffer, which is used for
//! displaying text on the screen in text mode. It includes color handling, cursor control,
//! and macros for printing and formatting text.

use core::fmt;
use lazy_static::lazy_static;
use spin::Mutex;
use volatile::Volatile;

use crate::cursor_control::update_cursor;

/// Enumeration of VGA text colors
#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGrey = 7,
    DarkGrey = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

/// Struct representing a color code
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct ColorCode(u8);

impl ColorCode {
    /// Creates a new `ColorCode` with specified foreground and background colors
    fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

/// Struct representing a screen character
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
struct ScreenChar {
    ascii_character: u8,
    color_code: ColorCode,
}

/// Constants defining the dimensions of the text buffer
const BUFFER_HEIGHT: usize = 25;
pub const BUFFER_WIDTH: usize = 80;

/// Struct representing the text buffer
#[repr(transparent)]
struct Buffer {
    chars: [[Volatile<ScreenChar>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

/// Struct representing the VGA text writer
pub struct Writer {
    column_position: usize,
    color_code: ColorCode,
    buffer: &'static mut Buffer,
}

impl Writer {
    /// Writes a byte to the text buffer
    pub fn write_byte(&mut self, byte: u8) {
        // Make sure we are not at the end of the line
        if self.column_position < BUFFER_WIDTH - 1 {
            // Put cursor one column ahead
            update_cursor(self.column_position + 1, BUFFER_HEIGHT - 1);
        }
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();
                }

                let row = BUFFER_HEIGHT - 1;
                let col = self.column_position;

                let color_code = self.color_code;
                self.buffer.chars[row][col].write(ScreenChar {
                    ascii_character: byte,
                    color_code,
                });
                // Make sure we are not at the end of the line
                if col < BUFFER_WIDTH - 1 {
                    // Set color of next character to make cursor color match
                    self.buffer.chars[row][col + 1].write(ScreenChar {
                        ascii_character: 0,
                        color_code,
                    });
                    self.column_position += 1;
                }
            }
        }
    }

    /// Sets the text color
    pub fn set_color(&mut self, fg_color: Color, bg_color: Color) {
        self.color_code = ColorCode::new(fg_color, bg_color);
        let color_code = self.color_code;
        self.buffer.chars[BUFFER_HEIGHT - 1][self.column_position].write(ScreenChar {
            ascii_character: 0,
            color_code,
        });
    }

    /// Handles a new line character
    fn new_line(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let character = self.buffer.chars[row][col].read();
                self.buffer.chars[row - 1][col].write(character);
            }
        }
        self.clear_row(BUFFER_HEIGHT - 1);
        self.column_position = 0;
    }

    /// Clears a row in the text buffer
    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_character: b' ',
            color_code: self.color_code,
        };
        for col in 0..BUFFER_WIDTH {
            self.buffer.chars[row][col].write(blank);
        }
    }

    /// Clears the entire screen
    pub fn clear_screen(&mut self) {
        let blank = ScreenChar {
            ascii_character: b' ',
            color_code: self.color_code,
        };
        for row in 0..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                self.buffer.chars[row][col].write(blank);
            }
        }
    }

    /// Writes a string to the text buffer
    pub fn write_string(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                _ => self.write_byte(0xfe),
            }
        }
    }

    /// Handles backspace key
    pub fn backspace(&mut self) {
        self.column_position -= 1;
        self.write_string(" ");
        self.column_position -= 1;
        update_cursor(self.column_position, BUFFER_HEIGHT - 1);
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

lazy_static! {
    /// Lazy static instance of the text writer
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer {
        column_position: 0,
        color_code: ColorCode::new(Color::Pink, Color::Black),
        buffer: unsafe { &mut *(0xb8000 as *mut Buffer) },
    });
}

/// Macro for printing text
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

/// Macro for handling backspace
#[macro_export]
macro_rules! backspace {
    () => {
        $crate::vga_buffer::_backspace()
    };
}

/// Macro for clearing the screen
#[macro_export]
macro_rules! clear_screen {
    () => {
        $crate::vga_buffer::_clear_screen()
    };
}

/// Macro for printing a new line
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)))
}

/// Macro for setting text color
#[macro_export]
macro_rules! set_color {
    ($fg_color:expr,$bg_color:expr) => {
        $crate::vga_buffer::_set_color($fg_color, $bg_color);
    };
}

/// Hidden function for setting text color
#[doc(hidden)]
pub fn _set_color(fg_color: Color, bg_color: Color) {
    use x86_64::instructions::interrupts;
    interrupts::without_interrupts(|| {
        WRITER.lock().set_color(fg_color, bg_color);
    });
}

/// Hidden function for printing
#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;
    interrupts::without_interrupts(|| {
        WRITER.lock().write_fmt(args).unwrap();
    });
}

/// Hidden function for handling backspace
#[doc(hidden)]
pub fn _backspace() {
    use x86_64::instructions::interrupts;
    interrupts::without_interrupts(|| {
        WRITER.lock().backspace();
    });
}

/// Hidden function for clearing the screen
#[doc(hidden)]
pub fn _clear_screen() {
    use x86_64::instructions::interrupts;
    interrupts::without_interrupts(|| {
        WRITER.lock().clear_screen();
    });
}
