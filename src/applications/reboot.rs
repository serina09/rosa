use core::arch::asm;

use crate::{
    command_alias,
    cpuio::{inb, outb},
};

use super::CommandMap;

/// Keyboard interface port.
const KBRD_INTRFC: u16 = 0x64;
/// Bit for keyboard data.
const KBRD_BIT_KDATA: u8 = 0;
/// Bit for user data.
const KBRD_BIT_UDATA: u8 = 1;
/// Keyboard I/O port.
const KBRD_IO: u16 = 0x60;
/// Keyboard reset command.
const KBRD_RESET: u8 = 0xFE;

/// Reboots the system using the keyboard controller.
///
/// # Safety
///
/// This function uses unsafe operations for low-level I/O and system control.
/// It disables interrupts, clears keyboard buffers, and triggers a CPU reset.
pub fn reboot(_: &[&str]) {
    unsafe {
        asm!("cli"); // disable all interrupts

        // Clear all keyboard buffers (output and command buffers)
        loop {
            let temp: u8 = inb(KBRD_INTRFC); // empty user data
            if check_flag(temp, KBRD_BIT_KDATA) != 0 {
                inb(KBRD_IO); // empty keyboard data
            }
            if check_flag(temp, KBRD_BIT_UDATA) == 0 {
                break;
            }
        }

        outb(KBRD_RESET, KBRD_INTRFC); // pulse CPU reset line

        loop {
            asm!("hlt"); // if that didn't work, halt the CPU
        }
    }
}

/// Checks a specific bit in a flags byte.
///
/// # Arguments
///
/// * `flags` - The flags byte to check.
/// * `n` - The bit position to check.
///
/// # Returns
///
/// Returns `1` if the bit is set, otherwise `0`.
fn check_flag(flags: u8, n: u8) -> u8 {
    flags & (1 << n)
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "reboot" => reboot
    }
}
