pub mod baboon_seeker;
pub mod calculator;
pub mod date;
pub mod debug;
pub mod io;
pub mod reboot;
pub mod rosafetch;
pub mod screen_locker;

type CommandMap = hashbrown::HashMap<&'static str, for<'a, 'b> fn(&'a [&'b str])>;

#[macro_export]
macro_rules! command_alias {
    ($($alias:expr => $func:path),*) => {{
        use hashbrown::HashMap;

        let mut map = HashMap::new();
        $(
            map.insert($alias, $func as for<'a, 'b> fn(&'a [&'b str]));
        )*
        map
    }};
}
