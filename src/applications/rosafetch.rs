use crate::{command_alias, print, println, set_color, vga_buffer::Color};

use super::CommandMap;

const KERNEL_VERSION: &str = env!("CARGO_PKG_VERSION");

/// Displays information about Rosa in a pfetch-like format.
pub fn main(_: &[&str]) {
    // Get uptime as seconds
    let uptime = crate::clock::uptime::get_uptime();

    // Calculate days, hours, minutes
    let days = uptime / 86400;
    let hours = (uptime % 86400) / 3600;
    let minutes = (uptime % 3600) / 60;

    set_color!(Color::LightRed, Color::Black);
    print!("    ###       ");
    set_color!(Color::Yellow, Color::Black);
    println!("user@rosa_system");
    set_color!(Color::LightRed, Color::Black);
    print!("  # ### #     os     ");
    set_color!(Color::White, Color::Black);
    println!("Rosa");
    set_color!(Color::LightRed, Color::Black);
    print!("### OOO ###   host   ");
    set_color!(Color::White, Color::Black);
    println!("x86_64");
    set_color!(Color::LightRed, Color::Black);
    print!("### OOO ###   kernel ");
    set_color!(Color::White, Color::Black);
    println!("{}-rosa", KERNEL_VERSION);
    set_color!(Color::LightRed, Color::Black);
    print!("  # ### #     uptime ");
    set_color!(Color::White, Color::Black);
    println!("{}d {}h {}m", days, hours, minutes);
    set_color!(Color::LightRed, Color::Black);
    print!("    ###       memory ");
    set_color!(Color::White, Color::Black);
    println!("Some");
    print!("\n");
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "rosafetch" => main
    }
}
