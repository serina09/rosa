use crate::{command_alias, println};

use super::CommandMap;

pub fn date(args: &[&str]) {
    if args.is_empty() {
        println!("{}", crate::clock::get_current_time());
    } else {
        println!(
            "{}",
            crate::clock::datetime::DateTime::from_unix_timestamp(args[0].parse::<u64>().unwrap())
        );
    }
}

pub fn udate(_: &[&str]) {
    println!("{}", crate::clock::get_current_time().to_unix_timestamp());
}

pub fn uptime(_: &[&str]) {
    crate::clock::uptime::print_uptime();
}

pub fn tz_command(args: &[&str]) {
    crate::clock::tz_command(args);
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "date" => date,
        "udate" => udate,
        "uptime" => uptime,
        "tz" => tz_command
    }
}
