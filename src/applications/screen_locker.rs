use crate::{clear_screen, command_alias, print, println, set_color, vga_buffer::Color};

use super::CommandMap;

/// Handles the main functionality for a password-protection.
///
/// # Arguments
///
/// * `command` - A vector containing the command and its arguments.
/// * `input_text` - A string containing the user's input text.
pub fn main(args: &[&str]) {
    if !args.is_empty() {
        // Extract password from input text
        let password = args.join(" ");

        // Clear the screen
        set_color!(Color::Black, Color::DarkGrey);
        clear_screen!();

        // Initialize password input variable
        let mut password_input: alloc::string::String = alloc::string::String::new();

        // Set text color to yellow
        set_color!(Color::Yellow, Color::DarkGrey);
        println!("This workstation is locked!");

        // Loop until the correct password is entered
        while password_input != password {
            // Set text color to white
            set_color!(Color::White, Color::DarkGrey);
            print!("Password: ");

            // Enable obscured input
            let mut guard = crate::interrupts::OBSCURE_INPUT.lock();
            *guard = true;
            drop(guard);

            // Get user input for the password
            password_input = crate::input("", Some([Color::White, Color::DarkGrey]));

            // Disable obscured input
            let mut guard = crate::interrupts::OBSCURE_INPUT.lock();
            *guard = false;
            drop(guard);

            // Reset text color to white
            set_color!(Color::White, Color::DarkGrey);
        }

        // Set text color to green
        set_color!(Color::Green, Color::Black);
        clear_screen!();
        println!("Unlocked!");
    } else {
        // Display a message if the command lacks a password
        println!("Requires password to lock with");
    }
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "lock" => main
    }
}
