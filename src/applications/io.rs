use crate::{clear_screen, command_alias, println};

use super::CommandMap;

pub fn echo(args: &[&str]) {
    println!("{}", args.join(" "))
}

pub fn clear(_: &[&str]) {
    clear_screen!()
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "echo" => echo,
        "clear" => clear
    }
}
