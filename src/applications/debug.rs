use crate::command_alias;

use super::CommandMap;

pub fn div_zero_fault(_: &[&str]) {
    unsafe {
        core::arch::asm!(
            "
            mov ax, 0
            mov bl, 0
            div bl
        "
        )
    };
}

pub fn pointer_fault(_: &[&str]) {
    unsafe {
        *(0xdeadbeaf as *mut u64) = 42;
    };
}

pub fn get_commands() -> CommandMap {
    command_alias! {
        "div_zero_fault" => div_zero_fault,
        "pointer_fault" => pointer_fault
    }
}
