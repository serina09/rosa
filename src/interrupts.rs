// Hardware Interrupt Handling Module
extern crate alloc;

use crate::{backspace, gdt, print, println};
use alloc::string::String;
use lazy_static::lazy_static;
use pc_keyboard::{layouts, DecodedKey, HandleControl, KeyCode, Keyboard, ScancodeSet1};
use pic8259::ChainedPics;
use spin::Mutex;
use x86_64::instructions::port::Port;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};

/// Enum of interrupt indices used for identifying specific interrupts.
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = PIC_1_OFFSET,
    Keyboard,
}

impl InterruptIndex {
    /// Convert the enum variant to u8.
    fn as_u8(self) -> u8 {
        self as u8
    }

    /// Convert the enum variant to usize.
    fn as_usize(self) -> usize {
        usize::from(self.as_u8())
    }
}

/// Offset for PIC 1, indicating the starting index for hardware interrupts.
pub const PIC_1_OFFSET: u8 = 32;

/// Offset for PIC 2, which is derived from PIC 1.
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

/// Mutex-protected global variable for chained PICs.
pub static PICS: spin::Mutex<ChainedPics> =
    spin::Mutex::new(unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) });

/// Mutex-protected input buffer for keyboard interrupts.
pub static INPUT_BUFFER: spin::Mutex<String> = spin::Mutex::new(String::new());

/// Mutex-protected boolean to hide keyboard inputs if needed
pub static OBSCURE_INPUT: spin::Mutex<bool> = Mutex::new(false);

lazy_static! {
    /// Unsafe global variable to indicate when user input is done.
    pub static ref USER_INPUT_DONE: Mutex<bool> = Mutex::new(false);
}

lazy_static! {
    /// Static reference to the Interrupt Descriptor Table (IDT).
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        idt.breakpoint.set_handler_fn(breakpoint_handler);
        unsafe {
            idt.double_fault
                .set_handler_fn(double_fault_handler)
                .set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX);
        }

        idt[InterruptIndex::Timer.as_usize()].set_handler_fn(timer_interrupt_handler);
        idt[InterruptIndex::Keyboard.as_usize()].set_handler_fn(keyboard_interrupt_handler);

        idt
    };
}

/// Initializes the Interrupt Descriptor Table (IDT).
pub fn init_idt() {
    IDT.load();
}

/// Breakpoint exception handler.
extern "x86-interrupt" fn breakpoint_handler(stack_frame: InterruptStackFrame) {
    println!("EXCEPTION: BREAKPOINT\n{:#?}", stack_frame);
}

/// Double fault exception handler.
extern "x86-interrupt" fn double_fault_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) -> ! {
    panic!("EXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
}

/// Timer interrupt handler.
extern "x86-interrupt" fn timer_interrupt_handler(_stack_frame: InterruptStackFrame) {
    // print!(".");

    unsafe {
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Timer.as_u8());
    }
}

/// Keyboard interrupt handler.
extern "x86-interrupt" fn keyboard_interrupt_handler(_stack_frame: InterruptStackFrame) {
    // Lazy static Mutex-protected Keyboard instance
    lazy_static! {
        static ref KEYBOARD: Mutex<Keyboard<layouts::Us104Key, ScancodeSet1>> = Mutex::new(
            Keyboard::new(layouts::Us104Key, ScancodeSet1, HandleControl::Ignore)
        );
    }

    let mut keyboard = KEYBOARD.lock();
    let mut port = Port::new(0x60);

    // Read scancode from port
    let scancode: u8 = unsafe { port.read() };

    // Process scancode and handle keyboard events
    if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
        if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(character) => {
                    match character {
                        // User is done inputting when they press enter
                        '\n' => {
                            let mut guard = USER_INPUT_DONE.lock();
                            *guard = true;
                        }
                        // Backspace
                        '\u{8}' => {
                            if INPUT_BUFFER.lock().len() > 0 {
                                // Remove character from screen
                                backspace!();
                                // Remove character from input buffer
                                INPUT_BUFFER.lock().pop();
                            }
                        }
                        // Print all other characters
                        _ => {
                            // Check if input should be obscured
                            if *OBSCURE_INPUT.lock() {
                                // If it should, print an asterisk
                                print!("*");
                            } else {
                                // If it shouldn't, print typed character
                                print!("{}", character);
                            }
                            // Add character to input buffer
                            INPUT_BUFFER.lock().push(character);
                        }
                    }
                }
                // Key was not able to be decoded as unicode
                DecodedKey::RawKey(key) => match key {
                    KeyCode::F12 => println!("{:?}", INPUT_BUFFER),
                    KeyCode::ArrowUp => {
                        // Check to make sure there is something in the history
                        if crate::HISTORY.lock().len() > 0 {
                            // Erase current input from screen
                            for _ in 0..INPUT_BUFFER.lock().len() {
                                backspace!();
                            }

                            // Get last input from history
                            let last_command = crate::HISTORY.lock().pop().unwrap();

                            // Replace input buffer
                            let mut state = INPUT_BUFFER.lock();
                            *state = last_command.clone();

                            // Put new text on screen
                            print!("{}", last_command);
                        }
                    }
                    _ => println!("WARNING: Undefined key {:?}", key),
                },
            }
        }
    }

    // Notify end of interrupt to the PIC
    unsafe {
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Keyboard.as_u8());
    }
}
